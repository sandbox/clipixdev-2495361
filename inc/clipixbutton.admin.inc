<?php
/**
 * @file
 * Admin functions for clipixbutton.
 */

/**
 * Settings for the dynamic Clipix "Save" button viewmode.
 */
function clipixbutton_dynamic_settings() {
  $form = array();
  $clipixbutton_node_options = node_type_get_names();
  $form['clipixbutton_dynamic_description'] = array(
    '#markup' => '<p>' . t('Configure the dynamic Clipix "Save" button. This Clipix "Save" button will save the URL you\'r visiting. You can set the content types on which the button displays, choose to display it in the content block or it\'s own block and set the appearance.') . '</p>',
  );
  $form['clipixbutton_dynamic_visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visibility settings'),
    '#collapsible' => FALSE,
  );
  $form['clipixbutton_dynamic_visibility']['clipixbutton_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display the Clipix "Save" button on these content types:'),
    '#options' => $clipixbutton_node_options,
    '#default_value' => variable_get('clipixbutton_node_types', array('article')),
    '#description' => t('Each of these content types will have the Clipix "Save" button automatically added to them.'),
  );
  $clipixbutton_full_node_display_desc = "If <em>Content area</em> is selected, the button will appear in the same area as the node content. If <em>Own block</em> is selected the Clipix SAVE button gets it\'s own block, which you can position at the "
                                           . l(t('block page'), 'admin/structure/block')
                                           . '. When you select <em>Links area</em> the Clipix "Save" button will be visible in the links area, usually at the bottom of the node (When you select this last option you may want to adjust the Appearance settings).';
  $form['clipixbutton_dynamic_visibility']['clipixbutton_full_node_display'] = array(
    '#type' => 'radios',
    '#title' => t('Where do you want to show the Clipix "Save" button (full node view)?'),
    '#options' => array(t('Content area'), t('Own block'), t('Links area')),
    '#default_value' => variable_get('clipixbutton_full_node_display', 0),
    '#description' => $clipixbutton_full_node_display_desc,
  );
  $form['clipixbutton_dynamic_visibility']['clipixbutton_teaser_display'] = array(
    '#type' => 'radios',
    '#title' => t('Where do you want to show the Clipix SAVE button on teasers?'),
    '#options' => array(
      t("Don\'t show on teasers"),
      t('Content area'),
      t('Links area'),
    ),
    '#default_value' => variable_get('clipixbutton_teaser_display', 0),
    '#description' => t('To show the Clipix SAVE button on teasers you can select the display area.'),
  );
  $form['clipixbutton_dynamic_appearance'] = array(
    '#type' => 'fieldset',
    '#title' => t('Appearance settings'),
    '#collapsible' => FALSE,
  );
  $form['clipixbutton_dynamic_appearance']['clipixbutton_size'] = array(
    '#type' => 'select',
    '#title' => t('Size:'),
    '#options' => array(
      'Small' => t('Small'),
      'Medium' => t('Medium'),
      'Large' => t('Large'),
    ),
    '#default_value' => variable_get('clipixbutton_size', 'Small'),
    '#description' => t('Determines the size of Clipix "Save" button.'),
  );
  $form['clipixbutton_dynamic_appearance']['clipixbutton_color'] = array(
    '#type' => 'select',
    '#title' => t('Color:'),
    '#options' => array(
      'Gray' => t('Gray'),
      'White' => t('White'),
      'Orange' => t('Orange'),
    ),
    '#default_value' => variable_get('clipixbutton_color', 'Gray'),
    '#description' => t('Determines the color of Clipix "Save" button.'),
  );
  $form['clipixbutton_dynamic_appearance']['clipixbutton_language'] = array(
    '#type' => 'select',
    '#title' => t('Language:'),
    '#options' => array(
      'en' => t('English'),
      'es' => t('Español'),
      'fr' => t('Français'),
      'he' => t('עברית'),
      'zh' => t('中文'),
      'it' => t('Italiano'),
      'tr' => t('Türkçe'),
      'de' => t('Deutsch'),
      'pt' => t('Português'),
      'ko' => t('한국어'),
      'ja' => t('日本語'),
      'ru' => t('Русский'),
    ),
    '#default_value' => variable_get('clipixbutton_language', 'en'),
    '#description' => t('Specific language to use. Default is English.'),
  );
  $form['clipixbutton_dynamic_appearance']['clipixbutton_hit_counter'] = array(
    '#type' => 'select',
    '#title' => t('Hit Counter:'),
    '#options' => array(
      'horizontal' => t('Horizontal'),
      'vertical' => t('vertical'),
    ),
    '#default_value' => variable_get('clipixbutton_hit_counter', 'horizontal'),
    '#description' => t('Determines the Clipix "Save" button position.'),
  );
  return system_settings_form($form);
}

README.txt
==========

Drupal Clipix "Save" Button module:
------------------------
Requires - Drupal 7
License - GPL (see LICENSE)


Overview:
--------
Rather than having to manually copy/paste the "Save" button code for each
piece of content you create, this module will automatically add that code to
the end of each chosen node type. Your visitors will have the option to save
your page to clipix.com as a clip.


Features:
---------
The Clipix "Save" Button module:

* Adds a "Save" button on your site
* Control the position of the button


Installation:
------------
Install this module in the usual way (drop the file into your site's contributed
modules directory).


Configuration:
-------------
Go to "Configuration" -> Clipix "Save" Button to find all the
configuration options.

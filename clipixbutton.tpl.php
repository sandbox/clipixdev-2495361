<?php
/**
 * @file
 * Template file for Clipix "Save" button.
 */
?>

<!-- Place this code where you want to display the button -->
<div id='ClipixWidgetApiContainer' cobrand='38'></div>

<!-- Place this code in the head section of your page or before the close body tag -->
<script type="text/javascript">
	var clipix_language = "<?php print $language; ?>";
	var clipix_size = "<?php print $size; ?>";
	var clipix_hitcounter = "<?php print $hit_counter; ?>";
	var clipix_color = "<?php print $color; ?>";
	var clipix_size_value = 1;

	  switch (clipix_color) {
		  case "Gray":
			  switch (clipix_size) {
				  case "Small":
					  clipix_size_value = 1;
					  break;
				  case "Medium":
					  clipix_size_value = 2;
					  break;
				  case "Large":
					  clipix_size_value = 3;
					  break;
			  }
			  break;
		  case "Orange":
			  switch (clipix_size) {
				  case "Small":
					  clipix_size_value = 4;
					  break;
				  case "Medium":
					  clipix_size_value = 5;
					  break;
				  case "Large":
					  clipix_size_value = 6;
					  break;
			  }
			  break;
		  case "White":
			  switch (clipix_size) {
				  case "Small":
					  clipix_size_value = 7;
					  break;
				  case "Medium":
					  clipix_size_value = 8;
					  break;
				  case "Large":
					  clipix_size_value = 9;
					  break;
			  }
			  break;
	  }

	  window._cxWidget = window._cxWidget || [];
	  _cxWidget.push(['version', '1.5']);
	  _cxWidget.push(['counter', 'on']);
	  _cxWidget.push(['language', clipix_language]);
	  _cxWidget.push(['iconType', '6' + clipix_size_value.toString()]);
	  _cxWidget.push(['shape', clipix_hitcounter]);

	  (function () {
		  var cxw = document.createElement('script');
		  cxw.type = 'text/javascript';
		  cxw.async = true;
		  cxw.src = '//widget.clipix.com/ScriptWidget.js';
		  var s = document.getElementsByTagName('script')[0];
		  s.parentNode.insertBefore(cxw, s);
	  })();
</script>
